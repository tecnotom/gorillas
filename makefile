# --------------------------------------------------------------------
# Makefile Genérico :: Módulo 2. Curso Experto Desarrollo Videojuegos
# Carlos González Morcillo     Escuela Superior de Informática (UCLM)
# --------------------------------------------------------------------
EXEC := main

DIRSRC := src/
DIROBJ := obj/
DIRHEA := include/
DIRCEGUI := /usr/local/include/CEGUI/
DIROGRE := RendererModules/Ogre/
DIRSDL := SDL/
DIRBULLET := /usr/local/include/bullet/
DIROBULL := /usr/local/include/OgreBullet/Dynamics/
DIROBULL2 := /usr/local/include/OgreBullet/Collisions/
DIRTERRAIN := OGRE/Terrain/

CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := -I $(DIRHEA) -I $(DIRBULLET) -I $(DIRHEA)$(DIRTERRAIN) -I $(DIRHEA)$(DIRSDL) -I $(DIRCEGUI) -I $(DIRCEGUI)$(DIROGRE) -I $(DIROBULL) -I $(DIROBULL2) -Wall `pkg-config --cflags OGRE --libs sdl`

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs-only-L OGRE --libs sdl`
LDLIBS := `pkg-config --libs-only-l OGRE --libs sdl` `pkg-config --libs-only-l OGRE --libs bullet` -lCEGUIBase -lCEGUIOgreRenderer -lCEGUIFalagardWRBase -lOgreBulletCollisions -lOgreBulletDynamics -lOIS -lGL -lstdc++ -lOgreMain -lSDL_image -lOgreTerrain -lSDL_mixer -L/usr/local/lib/ -L/usr/include/OGRE/ -lConvexDecomposition

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release) 
	CXXFLAGS += -O2 -D_RELEASE
else 
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(subst $(DIRSRC), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)*.cpp)))

.PHONY: all clean

all: info $(EXEC)

info:
	@echo '------------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])  '
	@echo '------------------------------------------------------'

# Enlazado -----------------------------------------------------------
$(EXEC): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# Compilación --------------------------------------------------------
$(DIROBJ)%.o: $(DIRSRC)%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(DIROBJ)*.o $(DIRSRC)*~ $(DIRHEA)*~ media/*/*~

edit:
	kate $(wildcard $(DIRSRC)*.cpp) $(wildcard $(DIRHEA)*.h) &
