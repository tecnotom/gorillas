  #################################
 #	CANNON MICRO-DEMO	 # 
#################################

	DEPENDENCIES
	############
	To compile this project you must have the following dependencies:

	OGRE 1.8.0 <http://www.ogre3d.org/>
	Cg Toolkit 3.0  <http://developer.nvidia.com/cg-toolkit/>
	OIS 1.3 <http://sourceforge.net/projects/wgois/>
	SDL 1.2 http://www.libsdl.org/projects/SDL_mixer/release-1.2.html
	CEGUI 0.8.3 http://cegui.org.uk/download
	Ogrebullet https://bitbucket.org/alexeyknyshev/ogrebullet
	Bullet 2.81-rev2613 http://code.google.com/p/bullet/

	INSTALLATION
	############
	  To install you will need to download the source from the bitbucket repository https://bitbucket.org/tecnotom/gorillas.git
	Git will be necessary to be installed, however.
	
	 
	  Write in the terminal the following commands inside the Memory folder:

	$ make
	$ ./main

	UNINSTALL
	###########
	  
	  For deleting all the binaries you exec the next command in the terminal:
	$ make clean

	GAME
	####

	   We can play 1 player or 2 players.
	  
	What we need to do is aim the ballon with the cannon and shoot to explode them.
	  
	Keyboard will be used to rotate the cannon and the angle of the inclination.
	After, We have to select the amount of gunpowder (Left Bar) for the strenght of the shoot and wait for the Reload time (Right bar) to shoot again.
		
	INSTRUCTIONS
	#############
	
	1 Player:
	Cursor Left/Right Cannon Rotation.
	Cursor Up/Down    Cannon Inclination.
	
	L Key: Amount of gunpowder (LEFT BAR)
	K Key: SHOOT
	G Key: Show collision mesh.
	H Key: Hide collision mesh.
	P Key: Pause/Resumen
	ESC Key: Escape / End
	 
	2 players:
	A/D Keys Left/Right Cannon Rotation.
	W/S Keys Up/Down    Cannon Inclination.
	B Key: Amount of gunpowder (LEFT BAR)
	V Key: Shoot 
		
	REPOSITORY
	###########

	  To download this project from the repository use the next command in the terminal:

	user@host:$ git clone https://bitbucket.org/tecnotom/gorillas.git

	NOTE: Git will be necessary to have installed as we mention earlier.
