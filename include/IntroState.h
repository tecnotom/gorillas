/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/  
  
#ifndef IntroState_H
#define IntroState_H
  
#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>  
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#include <OgreRenderWindow.h>
  
#include "GameState.h"
#include "HighScores.h"

class IntroState:public Ogre::Singleton < IntroState >, public GameState 
{

  public:
    IntroState ()  {} 
    
    void enter ();  
    void exit ();  
    void pause ();  
    void resume ();

    std::ostream& operator << (std::ostream &o);

    bool playPressed (const CEGUI::EventArgs & e);
    bool multiPlayPressed (const CEGUI::EventArgs & e);
    bool exit (const CEGUI::EventArgs & e);
    bool backmenu (const CEGUI::EventArgs & e);
    bool visibleSceneOptions (const CEGUI::EventArgs & e);
    bool viewScores (const CEGUI::EventArgs & e);
    bool viewCredits (const CEGUI::EventArgs & e);
    bool back (const CEGUI::EventArgs & e);
    bool submitScore (const CEGUI::EventArgs & e);
    bool restart (const CEGUI::EventArgs & e);
    bool resume (const CEGUI::EventArgs & e);
      
    void keyPressed (const OIS::KeyEvent & e);  
    void keyReleased (const OIS::KeyEvent & e); 
    void mouseMoved (const OIS::MouseEvent & e);  
    void mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id);  
    void mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id);  
    
    bool frameStarted (const Ogre::FrameEvent & evt);  
    bool frameEnded (const Ogre::FrameEvent & evt);  
    
    void createSceneInit ();
    void createSceneOptions ();
    void createSceneCredits ();
    void createSceneHighScores ();
    //void createSceneEnd ();
      

      
    
	// Heredados de Ogre::Singleton.
    static IntroState & getSingleton ();
      
    static IntroState *getSingletonPtr ();
      
    
    HighScores * getHighScores ()const
      {
	return
	  _pHighScores;
      };

    
  protected:
    Ogre::Root * _root;  
    Ogre::SceneManager * _sceneMgr;  
    Ogre::Viewport * _viewport;  
    Ogre::Camera * _camera;  
    
    bool _exitGame;
    
  private:
    void loadCEGUI ();  
    bool initSDL ();  
    
    CEGUI::Window * _sheetInit;  
    CEGUI::Window * _sheetCredits;
    CEGUI::Window * _sheetHighScores;
    //CEGUI::Window * _sheetEndGame;
      
    HighScores * _pHighScores;
    CEGUI::Window * _sheetConfig;
    CEGUI::Window * _sheetGame;
      
	//CEGUI::Window* _sheetEnd;
	//CEGUI::Window * _sheetPause;

    
};


 
#endif	/* 
 */
