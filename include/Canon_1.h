/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef Canon_1_H
#define Canon_1_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include <Shapes/OgreBulletCollisionsSphereShape.h>

#include "GameState.h"

class Canon_1
{
public:
  Canon_1 ();

  void cUp (Ogre::Real deltaT);
  void cDown (Ogre::Real deltaT);
  void cLeft (Ogre::Real deltaT);
  void cRight (Ogre::Real deltaT);
  Ogre::SceneNode* cShoot (OgreBulletDynamics::DynamicsWorld * _world, int &_r);

  float getTime ();
  void decTime (float deltaT);
  
  void resetCam();
  
  /*
     void keyReleased (const OIS::KeyEvent &e);

     void mouseMoved (const OIS::MouseEvent &e);
     void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
     void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

     bool frameStarted (const Ogre::FrameEvent& evt);
     bool frameEnded (const Ogre::FrameEvent& evt);

     // Heredados de Ogre::Singleton.
     static PlayState& getSingleton ();
     static PlayState* getSingletonPtr ();
   */

protected:
    Ogre::Root * _root;
    Ogre::SceneManager * _sceneMgr;
  //Ogre::Viewport* _viewport;
    Ogre::Camera * _camera;
    
    int _numEntities;
    float _timeLastObject;
    
    Ogre::SceneNode * _nCanon_1, *_nBase;
    Ogre::Real _rCanon_1, _r, _c, _rBase, _rCanon_12;
    Ogre::Real deltaT;
    Ogre::OverlayManager * _overlayManager;

private:
  //OgreBulletDynamics::DynamicsWorld * _world;
  //OgreBulletCollisions::DebugDrawer * _debugDrawer;


  //bool _Bpress, _Spress, _Uppress, _Downpress, _Rightpress, _Leftpress;

  enum TEDynamicObject
  {
    box,
    sheep
  };

    std::deque < OgreBulletDynamics::RigidBody * >_bodies;
    std::deque < OgreBulletCollisions::CollisionShape * >_shapes;

  //void CreateInitialWorld();
  //void AddDynamicObject(TEDynamicObject tObject);
};

#endif
