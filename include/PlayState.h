/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <list>

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "GameState.h"
#include "Canon.h"

class PlayState:public Ogre::Singleton < PlayState >, public GameState
{
public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();
  
  void resetCamera();
  
  std::ostream& operator << (std::ostream &o);

  void keyPressed (const OIS::KeyEvent & e);
  void keyReleased (const OIS::KeyEvent & e);

  void mouseMoved (const OIS::MouseEvent & e);
  void mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent & evt);
  bool frameEnded (const Ogre::FrameEvent & evt);

  int health;
  bool initSDL();

  // Heredados de Ogre::Singleton.
  static PlayState & getSingleton ();
  static PlayState * getSingletonPtr ();

protected:
  Ogre::Root * _root;
  Ogre::SceneManager * _sceneMgr;
  Ogre::Viewport * _viewport;
  Ogre::Camera * _camera;

  bool _exitGame;
  bool cameraShoot;
  
  Ogre::SceneNode* _plano;
  Ogre::SceneNode* _bala;
  
  SoundFXPtr _simpleEffect;
  SoundFXPtr _simpleEffect2;

private:
  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;
  int _numEntities;
  //float _timeLastObject;
  //Ogre::SceneNode *_nCanon, *_nBase;
  //Ogre::Real _rCanon, _r, _c, _rBase;
  Ogre::Real deltaT;
  Ogre::OverlayManager * _overlayManager;
  Canon* _canon;
    
  std::list<Ogre::SceneNode*> _lSceneNode;
  std::list<Ogre::SceneNode*>::iterator _iSceneNode;

  bool
    _Bpress,
    _Spress,
    _Uppress,
    _Downpress,
    _Rightpress,
    _Leftpress,
    _Lpress;
  float maxHealth;

  std::deque < OgreBulletDynamics::RigidBody * >_bodies;
  std::deque < OgreBulletCollisions::CollisionShape * >_shapes;

  Ogre::TerrainGlobalOptions * mTerrainGlobals;
  Ogre::TerrainGroup * mTerrainGroup;

  void CreateInitialWorld ();
  void DetectCollisionDrain();
  void compEnd();

  void createHUD ();
  void updateHUD ();
};

#endif
