/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "MultiPlayState.h"
#include "PauseState.h"
#include "EndState.h"
#include "EndStateM1.h"
#include "EndStateM2.h"

#include <iostream>

using namespace std;

int
main ()
{

  GameManager *game = new GameManager ();
  IntroState *introState = new IntroState ();
  PlayState *playState = new PlayState ();
  MultiPlayState *multiPlayState = new MultiPlayState ();
  PauseState *pauseState = new PauseState ();
  EndState *endState = new EndState ();
  EndStateM1 *endStateM1 = new EndStateM1 ();
  EndStateM2 *endStateM2 = new EndStateM2 ();

  UNUSED_VARIABLE (introState);
  UNUSED_VARIABLE (playState);
  UNUSED_VARIABLE (multiPlayState);
  UNUSED_VARIABLE (pauseState);
  UNUSED_VARIABLE (endState);
  UNUSED_VARIABLE (endStateM1);
  UNUSED_VARIABLE (endStateM2);

  try
  {
    // Inicializa el juego y transición al primer estado.
    game->start (IntroState::getSingletonPtr ());
  }
  catch (Ogre::Exception & e)
  {
    std::cerr << "Excepción detectada: " << e.getFullDescription ();
  }

  delete game;

  return 0;
}
