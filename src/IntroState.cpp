#include "IntroState.h"
#include "PlayState.h"
#include "MultiPlayState.h"

#include <SDL.h>
#include <SDL/SDL_mixer.h>
  
template <> IntroState * Ogre::Singleton < IntroState >::msSingleton = 0;

 
 
void 
IntroState::enter () 
{
  _root = Ogre::Root::getSingletonPtr ();
  _sheetGame = 0;
  loadCEGUI ();
  initSDL ();
  createSceneInit ();
  createSceneOptions ();
  createSceneCredits ();
  createSceneHighScores ();
  //createSceneEnd ();
  _pHighScores = new HighScores ();
  _exitGame = false; 
} 
 
 
 
void 


IntroState::createSceneInit () 
{
  
  CEGUI::SchemeManager::getSingleton ().create ("TaharezLook.scheme");
  CEGUI::System::getSingleton ().setDefaultFont ("DejaVuSans-10");
  CEGUI::System::getSingleton ().setDefaultMouseCursor ("TaharezLook", "MouseArrow");
  CEGUI::ImagesetManager::getSingleton ().createFromImageFile ("MenuBackground","Cannons_Cover.png");
  CEGUI::ImagesetManager::getSingleton ().createFromImageFile ("MenusBG","Cannons_DimCover.png");

  _sheetInit =CEGUI::WindowManager::getSingleton ().createWindow ("TaharezLook/StaticImage", "Init");
  _sheetInit->setArea (CEGUI::URect (cegui_reldim (0), cegui_reldim (0),cegui_reldim (1),cegui_reldim (1)));
  _sheetInit->setProperty ("FrameEnabled", "false");
  
 
 
    //_sheetInit->setProperty("BackgroundEnabled", "false");
    _sheetInit->setProperty ("Image", "set:MenuBackground image:full_image");
  
  CEGUI::Window * layout = CEGUI::WindowManager::getSingleton ().loadWindowLayout ("Memory.layout");
  CEGUI::Window * exitButton = CEGUI::WindowManager::getSingleton ().getWindow ("Exit");
  exitButton->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState::exit, this));
  CEGUI::Window * playButton = CEGUI::WindowManager::getSingleton ().getWindow ("Play");
  playButton->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState::playPressed,this));
  CEGUI::Window * configButton = CEGUI::WindowManager::getSingleton ().getWindow ("Configuration");
  configButton->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState:: multiPlayPressed, this));
  CEGUI::Window * credits = CEGUI::WindowManager::getSingleton ().getWindow ("Credits");
  credits->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState:: viewCredits, this));
  CEGUI::Window * scores = CEGUI::WindowManager::getSingleton ().getWindow ("Scores");
  scores->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState:: viewScores, this));
  _sheetInit->addChildWindow (layout);
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
  _sheetInit->setVisible (true);
  
  //if (_mainTrack->isPlaying()) {}
  //  _mainTrack->unload();
  _mainTrack = _pTrackManager->load ("CannonIntro.ogg");
  _mainTrack->play ();
} 
 
void 

IntroState::createSceneOptions () 
{
  
 
_sheetConfig = 
CEGUI::WindowManager::getSingleton (). 
    createWindow ("TaharezLook/StaticImage", "Config");
  
 
_sheetConfig->
    
 
setArea (CEGUI::
		
 
URect (cegui_reldim (0), cegui_reldim (0),
			  cegui_reldim (1), 
 
cegui_reldim (1)));
  
 
_sheetConfig->setProperty ("FrameEnabled", "false");
  
 
_sheetConfig->setProperty ("Image", "set:MenusBG image:full_image");
  
 
 
CEGUI::Window * layout = 
CEGUI::WindowManager::getSingleton (). 
    loadWindowLayout ("Memory_config.layout");
  
 
 
CEGUI::Window * backButton =
    
 
CEGUI::WindowManager::getSingleton ().getWindow ("Back");
  
 
backButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				 
 
CEGUI::Event::Subscriber (&IntroState::
							      back, 
 
this));
  
 
 
CEGUI::RadioButton * radioButton = (CEGUI::RadioButton *) CEGUI::WindowManager::getSingleton ().getWindow ("easy");
  
 
radioButton->setSelected (true);
  
 
_sheetConfig->addChildWindow (layout);
  
 
 
_sheetConfig->setVisible (false);

 
 
 
 
} 
 
void 

IntroState::createSceneCredits () 
{
  
 
_sheetCredits = 
CEGUI::WindowManager::getSingleton (). 
    createWindow ("TaharezLook/StaticImage", "Credit");
  
 
_sheetCredits->
    
 
setArea (CEGUI::
		
 
URect (cegui_reldim (0), cegui_reldim (0),
			  cegui_reldim (1), 
 
cegui_reldim (1)));
  
 
_sheetCredits->setProperty ("FrameEnabled", "false");
  
 
_sheetCredits->setProperty ("Image", "set:MenusBG image:full_image");
  
 
 
CEGUI::Window * layout = 
CEGUI::WindowManager::getSingleton (). 
    loadWindowLayout ("Memory_credits.layout");
  
 
 
CEGUI::Window * backCre =
    
 
CEGUI::WindowManager::getSingleton ().getWindow ("BackCredits");
  
 
backCre->subscribeEvent (CEGUI::PushButton::EventClicked,
			      
 
CEGUI::Event::Subscriber (&IntroState::back,
							   
 
this));
  
 
_sheetCredits->addChildWindow (layout);
  
 
 
_sheetCredits->setVisible (false);

 
 
 
 
} 
 
 

//<JRC: crea la GUI para mostrar tabla de records
void 
IntroState::createSceneHighScores () 
{
  
 
 
_sheetHighScores = 
CEGUI::WindowManager::getSingleton (). 
    createWindow ("TaharezLook/StaticImage", "HighScores");
  
 
_sheetHighScores->
    
 
setArea (CEGUI::
		
 
URect (cegui_reldim (0), cegui_reldim (0),
			  cegui_reldim (1), 
 
cegui_reldim (1)));
  
 
_sheetHighScores->setProperty ("FrameEnabled", "false");
  
 
_sheetHighScores->setProperty ("Image", "set:MenusBG image:full_image");
  
 
 
CEGUI::Window * layout = 
CEGUI::WindowManager::getSingleton (). 
    loadWindowLayout ("HighScores.layout");
  
 
 
CEGUI::Window * exitScoresButton =
    
 
CEGUI::WindowManager::
    
getSingleton (). 
 getWindow ("HighScoresRoot/Wrapper/ExitButton");
  
 
exitScoresButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				       
 
CEGUI::Event::
				       Subscriber (&IntroState::
 
back,
						   this));
  
 
_sheetHighScores->addChildWindow (layout);
  
 
 
_sheetHighScores->setVisible (false);

 
 
} 
 
 

//JRC crea GUI para insertar nombre y puntuacion al terminar la partida


 
 
 
 
void IntroState::loadCEGUI () 
{
  CEGUI::OgreRenderer * renderer = &CEGUI::OgreRenderer::bootstrapSystem ();

  CEGUI::Scheme::setDefaultResourceGroup ("Schemes");
  CEGUI::Imageset::setDefaultResourceGroup ("Imagesets");
  CEGUI::Font::setDefaultResourceGroup ("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup ("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup ("LookNFeel"); 
} 
 
bool IntroState::initSDL () 
{
  _pTrackManager = new TrackManager;
  _pSoundFXManager = new SoundFXManager;
  if (SDL_Init (SDL_INIT_AUDIO) < 0)
    return false;
  atexit (SDL_Quit);
  // Inicializando SDL mixer...
  if (Mix_OpenAudio (MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0)
    return false;
  // Llamar a Mix_CloseAudio al terminar.
  atexit (Mix_CloseAudio);
  return true;
}


 
 
 
bool 
 IntroState::exit (const CEGUI::EventArgs & e) 
{ 
    //_sceneMgr->clearScene();
    _root->getAutoCreatedWindow ()->removeAllViewports ();
  
 
_exitGame = true;
  
 
return true;

 
}


 
 
 
bool IntroState::back (const CEGUI::EventArgs & e) 
{
  //CEGUI::RadioButton * radioButton = (CEGUI::RadioButton *) CEGUI::WindowManager::getSingleton().getWindow ("easy");
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
  this->_sheetConfig->setVisible (false);
  _sheetInit->setVisible (true);
  return true; 
}


 
 
 
bool 
 IntroState::resume (const CEGUI::EventArgs & e) 
{
  _mainTrack = _pTrackManager->load ("CannonIntro.ogg");
  _mainTrack->play ();
  _sheetGame->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetGame);

  return true;
}


 
 
 
 
 
bool IntroState::submitScore (const CEGUI::EventArgs & e) 
{
  return true; 
}


 
 
 
bool IntroState::multiPlayPressed(const CEGUI::EventArgs & e) {
  // Transición al siguiente estado.
  _sheetInit->setVisible (false);
  pushState(MultiPlayState::getSingletonPtr());
  return true;
}
 
bool 
 IntroState::restart (const CEGUI::EventArgs & e) 
{
  
 
    //this->_gameState->setState (INIT);
    //this->createSceneGame (e);
    return true;

 
}


 
 
 
 
bool 
 IntroState::viewCredits (const CEGUI::EventArgs & e) 
{
  _sheetInit->setVisible (false);
  _sheetCredits->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetCredits);
  return true;
}


 
 
 
bool 
 IntroState::visibleSceneOptions (const CEGUI::EventArgs & e) 
{
  
 
    //_gameState->setState (CONFIG);
    _sheetInit->setVisible (false);
  
 
_sheetConfig->setVisible (true);
  
 
CEGUI::System::getSingleton ().setGUISheet (_sheetConfig);
  
 
return true;

 
}


 
 
 
 
bool 
 IntroState::viewScores (const CEGUI::EventArgs & e) 
{
  
 
_sheetInit->setVisible (false);
  
 
_sheetHighScores->setVisible (true);
  
 
CEGUI::System::getSingleton ().setGUISheet (_sheetHighScores);
  
 
    //acceso a la ventana que muestra el texto
    CEGUI::Window * showText = 
CEGUI::WindowManager::getSingleton (). 
    getWindow ("HighScoresRoot/Wrapper/scoresWindow");
  
 
showText->setText ("");	//resetear texto porque sino cada vez que se entra en viewScores se duplica lo que se muestra lista
  getHighScores ()->read ();
  
 
    //imprimir la lista de las puntuaciones
    std::list < string > scores = getHighScores ()->getScoresList ();
  
 
list < string >::iterator it;
  
 
 
for (it = scores.begin (); it != scores.end (); ++it)
    
 
    {
      
 
showText->appendText (*it);
    
 
}
  
 
 
return true;

 
}


 
 
 
 
 
 
bool 
 IntroState::backmenu (const CEGUI::EventArgs & e) 
{
  
 
    //_gameState->setState (INIT);
    _sheetInit->setVisible (true);
  
 
//_sheetPause->setVisible (false);
    
CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
  
 
//_mainTrack->unload ();
  
 
_mainTrack = _pTrackManager->load ("CannonIntro.ogg");
  
 
_mainTrack->play ();
  
 
    //_sceneMgr->getRootSceneNode ()->setVisible (false);
    return true;

 
}


 
 
 
 
void IntroState::exit () {
  //_mainTrack->unload ();
  _root->getAutoCreatedWindow ()->removeAllViewports ();
  _exitGame = true;
} 
 
void IntroState::pause () {} 
 
void IntroState::resume () 
{
  _mainTrack = _pTrackManager->load ("CannonIntro.ogg");
  _mainTrack->play ();
  _sheetInit->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
} 
 
bool IntroState::frameStarted (const Ogre::FrameEvent & evt) {
 return true;
}
 
bool IntroState::frameEnded (const Ogre::FrameEvent & evt) {
  if (_exitGame) {
    return false;
  }
  return true;
}


 
 
 
void 
IntroState::keyPressed 
 (const OIS::KeyEvent & e) 
{
  
 
    // Transición al siguiente estado.
    // Espacio --> PlayState
    if (e.key == OIS::KC_SPACE)
    
    {
    
 
 
}

 
}


 
 
 
bool IntroState::playPressed (const CEGUI::EventArgs & e) 
{
    // Transición al siguiente estado.
    _sheetInit->setVisible (false);
    pushState (PlayState::getSingletonPtr ());
return true;
}


 
 
 
void 
IntroState::keyReleased 
 (const OIS::KeyEvent & e) 
{
  
 
 
CEGUI::System::getSingleton ().injectKeyUp (e.key);
  
 
 
if (e.key == OIS::KC_ESCAPE) {
  _exitGame = true;
    
 
}

 
}


 
 
 
void 
IntroState::mouseMoved 
 (const OIS::MouseEvent & e) 
{

 
} 
 
void 


IntroState::mousePressed 
 (const OIS::MouseEvent & e, OIS::MouseButtonID id) 
{

 
} 
 
void 


IntroState::mouseReleased 
 (const OIS::MouseEvent & e, OIS::MouseButtonID id) 
{

 
} 
 
IntroState * 
IntroState::getSingletonPtr () 
{
  
 
return msSingleton;

 
}


 
 
 
IntroState & 
IntroState::getSingleton () 
{
  
 
assert (msSingleton);
  
 
return *msSingleton;

 
}

std::ostream& IntroState::operator << (std::ostream &o)
{
 o << "IntroState 1\n"; 
  return o;
}
 
 
