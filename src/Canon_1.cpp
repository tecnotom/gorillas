#include "Canon_1.h"
//#include "PauseState.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

using namespace Ogre;



Canon_1::Canon_1 ()
{

  _root = Ogre::Root::getSingletonPtr ();
  _sceneMgr = _root->getSceneManager ("SceneManager");
  _camera = _sceneMgr->getCamera ("IntroCamera");
  _timeLastObject = 0;

  _numEntities = 0;		// Numero de Shapes instanciadas
  //_timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto  
  _rCanon_1 = 0;
  _rCanon_12 = 0;
  _r = 15;
  _c = 1.5;
  _rBase = 0;
  // Creamos y anadimos el canyon
  //Vector3 position = Vector3 (0, 0, 50);
  Entity *entity = NULL;	// Base del canyon
  entity = _sceneMgr->createEntity ("Canon_1_Base", "canon/Base.mesh");

  SceneNode *node2 = _sceneMgr->getRootSceneNode ()->createChildSceneNode ();
  node2->attachObject (entity);
  node2->translate (0, 0, 50);
  //node2->
  //node2->setScale(3,3,3);
  // Soportes del canyon
  entity = _sceneMgr->createEntity ("Canon_1", "canon/Canon.mesh");
  SceneNode *node3 = node2->createChildSceneNode ();
  node3->attachObject (entity);
  node3->translate (0, 1.7, 0);
  _nCanon_1 = node3;
  _nBase = node2;


}

void
Canon_1::cUp (Ogre::Real deltaT)
{
  if (_rCanon_1 < 55)
    {
      _rCanon_1 += _r * deltaT;
      _rCanon_12 +=  _c * deltaT;
      _camera->pitch (Ogre::Degree (_r * deltaT / 3.0));
      _camera->moveRelative (Vector3 (0, -_c * deltaT, 0));
      _nCanon_1->pitch (Ogre::Degree (_r * deltaT));
    }

}

void
Canon_1::cDown (Ogre::Real deltaT)
{
  if (_rCanon_1 > 0)
    {
      _rCanon_1 -= _r * deltaT;
      _rCanon_12 -=  _c * deltaT;
      _camera->pitch (Ogre::Degree (_r * deltaT / -3.0));
      _camera->moveRelative (Vector3 (0, _c * deltaT, 0));
      _nCanon_1->pitch (Ogre::Degree (-_r * deltaT));
    }
}

void
Canon_1::cLeft (Ogre::Real deltaT)
{
  if (_rBase < 30)
    {
      _rBase += _r * deltaT / 1.5;
      _camera->yaw (Ogre::Degree (_r * deltaT / 6.0));
      //_camera->moveRelative(Vector3(0,_c * deltaT, 0));
      _nBase->yaw (Ogre::Degree (_r * deltaT / 1.5));
    }

}




void
Canon_1::cRight (Ogre::Real deltaT)
{
  if (_rBase > -30)
    {
      _rBase -= _r * deltaT / 1.5;
      _camera->yaw (Ogre::Degree (_r * deltaT / -6.0));
      //_camera->moveRelative(Vector3(0,-_c * deltaT, 0));
      _nBase->yaw (Ogre::Degree (_r * deltaT / -1.5));
    }




}

SceneNode*
Canon_1::cShoot (OgreBulletDynamics::DynamicsWorld * _world, int &_r)
{
  _timeLastObject = 5.0;	// Segundos para anadir uno nuevo... 
  
  _camera->detachFromParent(); 
  //_bala->removeAndDestroyAllChildren();  
  _camera->setPosition (Ogre::Vector3 (0, 10, 30));
  //_camera->lookAt (Ogre::Vector3 (0, 0, 0));
  _camera->moveRelative (Vector3 (0, -_rCanon_12, 0));
  //_bala = NULL;

  //Vector3 size = Vector3::ZERO;
  Ogre::Real size2 = 0;
  Vector3 position = Vector3 (0, 1.4, 49);


  Entity *entity = NULL;
  entity =
    _sceneMgr->createEntity ("Box" + StringConverter::toString (_numEntities),
			     "Icosphere.mesh");
  entity->setMaterialName ("cube");

  SceneNode *node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ();
  node->attachObject (entity);
  node->scale (0.4, 0.4, 0.4);
  //node->addChild(_camera);
  SceneNode *nodeCam = node->createChildSceneNode();
  nodeCam->attachObject(_camera);

  //OgreBulletCollisions::StaticMeshToShapeConverter * trimeshConverter = NULL;
  OgreBulletCollisions::CollisionShape * bodyShape = NULL;
  OgreBulletDynamics::RigidBody * rigidBody = NULL;


  AxisAlignedBox boundingB = entity->getBoundingBox ();
  //size = boundingB.getSize ();
  size2 = 1.0 * 0.4;		// El tamano en Bullet se indica desde el centro
  bodyShape = new OgreBulletCollisions::SphereCollisionShape(size2);

  rigidBody =
    new OgreBulletDynamics::RigidBody ("rigidBody" +
				       StringConverter::
				       toString (_numEntities), _world);

  rigidBody->setShape (node, bodyShape,
		       0.55 /* Restitucion */ , 1.2 /* Friccion */ ,
		       55.0 /* Masa */ , position /* Posicion inicial */ ,
		       Quaternion::IDENTITY /* Orientacion */ );


  // Impulso inicial!!!!!!
  //Real _r = 20.0;             // Fuerza inicial
  Real PI_180 = 0.0174532888;	// Conversion radianes
  Vector3 impulso2 = Vector3 (-_r * sin ((90 - _rCanon_1) * PI_180) * sin (_rBase * PI_180),	// X
			      _r * cos ((90 - _rCanon_1) * PI_180),	// Y
			      -_r * sin ((90 - _rCanon_1) * PI_180) * cos (_rBase * PI_180));	// Z
  rigidBody->setLinearVelocity (impulso2);

  _numEntities++;

  // Anadimos los objetos a las deques
  _shapes.push_back (bodyShape);
  _bodies.push_back (rigidBody);
  _r = 0;
  
  return (node);
}

void
Canon_1::decTime (float deltaT)
{
  _timeLastObject -= deltaT;
}

float
Canon_1::getTime ()
{
  return _timeLastObject;
}

void Canon_1::resetCam()
{
  _camera->yaw (Ogre::Degree (_rBase / 4.0));
  _camera->pitch (Ogre::Degree (_rCanon_1 / 3.0));
  //_camera->moveRelative (Vector3 (0, -_c * deltaT, 0));
  _camera->moveRelative (Vector3 (0, -_rCanon_12, 0));
  
}
