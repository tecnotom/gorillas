#include "PauseState.h"

template <> PauseState * Ogre::Singleton < PauseState >::msSingleton = 0;

void
PauseState::enter ()
{
  _root = Ogre::Root::getSingletonPtr ();

  
  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  Ogre::Overlay * overlay = pOverlayMgr->getByName ("PAUSE");
  //para hacer el calculo de cuanta barra de vida mostrar de acorde a la vida del jugador
  //Ogre::OverlayElement * elem = pOverlayMgr->getOverlayElement ("Pause");
  overlay->show ();

  //createScenePause ();

  _exitGame = false;
}

void
PauseState::exit ()
{
  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  Ogre::Overlay * overlay = pOverlayMgr->getByName ("PAUSE");
  overlay->hide ();
}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{

}

bool PauseState::frameStarted (const Ogre::FrameEvent & evt)
{
  return true;
}

bool PauseState::frameEnded (const Ogre::FrameEvent & evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
PauseState::keyPressed (const OIS::KeyEvent & e)
{
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P)
    {
      popState ();
    }
}

void
PauseState::keyReleased (const OIS::KeyEvent & e)
{
}

void
PauseState::mouseMoved (const OIS::MouseEvent & e)
{
}

void
PauseState::mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

void
PauseState::mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

PauseState *
PauseState::getSingletonPtr ()
{
  return msSingleton;
}

PauseState & PauseState::getSingleton ()
{
  assert (msSingleton);
  return *msSingleton;
}


std::ostream& PauseState::operator << (std::ostream &o)
{
 o << "PauseState 1\n"; 
  return o;
}

