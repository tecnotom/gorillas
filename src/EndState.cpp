#include "EndState.h"
#include "PlayState.h"

template <> EndState * Ogre::Singleton < EndState >::msSingleton = 0;

void
EndState::enter ()
{
  
  _root = Ogre::Root::getSingletonPtr ();
  if (!created) {
    createSceneEnd();
  }
  else {
    _sheetEnd->setVisible (true);
  }
  CEGUI::System::getSingleton ().setGUISheet (_sheetEnd);
  _exitGame = false;
}

void
EndState::exit ()
{
  _sheetEnd->setVisible (false);
}

void
EndState::pause ()
{
}

void
EndState::resume ()
{

}

bool EndState::frameStarted (const Ogre::FrameEvent & evt)
{
  return true;
}

bool EndState::frameEnded (const Ogre::FrameEvent & evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
EndState::keyPressed (const OIS::KeyEvent & e)
{
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P)
    {
      _sheetEnd->setVisible (false);
      popState ();
    }
}

void
EndState::keyReleased (const OIS::KeyEvent & e)
{
}

void
EndState::mouseMoved (const OIS::MouseEvent & e)
{
}

void
EndState::mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

void
EndState::mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

EndState *EndState::getSingletonPtr ()
{
  return msSingleton;
}

EndState &EndState::getSingleton ()
{
  assert (msSingleton);
  return *msSingleton;
}







void 
EndState::createSceneEnd () 
{
  created = true; 
  CEGUI::ImagesetManager::getSingleton ().createFromImageFile ("EndBG", "YouWinEndGame.png");
  _sheetEnd = CEGUI::WindowManager::getSingleton ().createWindow ("TaharezLook/StaticImage", "End");
  _sheetEnd->setArea (CEGUI::URect (cegui_reldim (0), cegui_reldim (0), cegui_reldim (1), cegui_reldim (1)));
  _sheetEnd->setProperty ("FrameEnabled", "false");
  //_sheetEnd->setProperty("BackgroundEnabled", "true");
  _sheetEnd->setProperty ("Image", "set:EndBG image:full_image");
  CEGUI::Window * layout = CEGUI::WindowManager::getSingleton ().loadWindowLayout ("InsertScore.layout");
  //CEGUI::Window * ContinueButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Continue");  
  //ContinueButton->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState::submitScore,this));
  CEGUI::Window * MainMenuButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Menu"); 
  MainMenuButton->subscribeEvent (CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber (&EndState::backmenu,this));
  CEGUI::Window * restartButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Continue");
  restartButton->subscribeEvent (CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber (&EndState::restart,this));
  
  _sheetEnd->addChildWindow (layout);
  _sheetEnd->setVisible (true);
} 

std::ostream& EndState::operator << (std::ostream &o)
{
 o << "Endstate 1\n"; 
  return o;
}


bool EndState::backmenu (const CEGUI::EventArgs & e) 
{ 
    // Transición al siguiente estado.
      _sheetEnd->setVisible (false);
      popState ();
  //pushState (PlayState::getSingletonPtr ());
return true;
}

bool EndState::restart (const CEGUI::EventArgs & e) 
{ 
    // Transición al siguiente estado.
      _sheetEnd->setVisible (false);
      //popState ();
      changeState(PlayState::getSingletonPtr ());
return true;
}