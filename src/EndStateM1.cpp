#include "EndStateM1.h"
#include "MultiPlayState.h"

template <> EndStateM1 * Ogre::Singleton < EndStateM1 >::msSingleton = 0;

void
EndStateM1::enter ()
{
  
  _root = Ogre::Root::getSingletonPtr ();
  if (!created) {
    createSceneEnd();
  }
  else {
    _sheetEnd1->setVisible (true);
  }
  CEGUI::System::getSingleton ().setGUISheet (_sheetEnd1);
  _exitGame = false;
}

void
EndStateM1::exit ()
{
  _sheetEnd1->setVisible (false);
}

void
EndStateM1::pause ()
{
}

void
EndStateM1::resume ()
{

}

bool EndStateM1::frameStarted (const Ogre::FrameEvent & evt)
{
  return true;
}

bool EndStateM1::frameEnded (const Ogre::FrameEvent & evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
EndStateM1::keyPressed (const OIS::KeyEvent & e)
{
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P)
    {
      _sheetEnd1->setVisible (false);
      popState ();
    }
}

void
EndStateM1::keyReleased (const OIS::KeyEvent & e)
{
}

void
EndStateM1::mouseMoved (const OIS::MouseEvent & e)
{
}

void
EndStateM1::mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

void
EndStateM1::mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

EndStateM1 *EndStateM1::getSingletonPtr ()
{
  return msSingleton;
}

EndStateM1 &EndStateM1::getSingleton ()
{
  assert (msSingleton);
  return *msSingleton;
}







void 
EndStateM1::createSceneEnd () 
{
  created = true; 
  CEGUI::ImagesetManager::getSingleton ().createFromImageFile ("EndBG1", "Player1Wins.png");
  _sheetEnd1 = CEGUI::WindowManager::getSingleton ().createWindow ("TaharezLook/StaticImage", "End1");
  _sheetEnd1->setArea (CEGUI::URect (cegui_reldim (0), cegui_reldim (0), cegui_reldim (1), cegui_reldim (1)));
  _sheetEnd1->setProperty ("FrameEnabled", "false");
  //_sheetEnd1->setProperty("BackgroundEnabled", "true");
  _sheetEnd1->setProperty ("Image", "set:EndBG1 image:full_image");
  CEGUI::Window * layout = CEGUI::WindowManager::getSingleton ().loadWindowLayout ("InsertScore1.layout");
  //CEGUI::Window * ContinueButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Continue");  
  //ContinueButton->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState::submitScore,this));
  CEGUI::Window * MainMenuButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS1/GameOver/Menu"); 
  MainMenuButton->subscribeEvent (CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber (&EndStateM1::backmenu,this));
  CEGUI::Window * restartButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS1/GameOver/Continue");
  restartButton->subscribeEvent (CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber (&EndStateM1::restart,this));
  
  _sheetEnd1->addChildWindow (layout);
  _sheetEnd1->setVisible (true);
} 

std::ostream& EndStateM1::operator << (std::ostream &o)
{
 o << "Endstate 1\n"; 
  return o;
}


bool EndStateM1::backmenu (const CEGUI::EventArgs & e) 
{ 
    // Transición al siguiente estado.
      _sheetEnd1->setVisible (false);
      popState ();
  //pushState (PlayState::getSingletonPtr ());
return true;
}

bool EndStateM1::restart (const CEGUI::EventArgs & e) 
{ 
    // Transición al siguiente estado.
      _sheetEnd1->setVisible (false);
      //popState ();
      changeState(MultiPlayState::getSingletonPtr ());
return true;
}