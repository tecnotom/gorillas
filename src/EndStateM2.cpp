#include "EndStateM2.h"
#include "MultiPlayState.h"

template <> EndStateM2 * Ogre::Singleton < EndStateM2 >::msSingleton = 0;

void
EndStateM2::enter ()
{
  
  _root = Ogre::Root::getSingletonPtr ();
  if (!created) {
    createSceneEnd();
  }
  else {
    _sheetEnd2->setVisible (true);
  }
  CEGUI::System::getSingleton ().setGUISheet (_sheetEnd2);
  _exitGame = false;
}

void
EndStateM2::exit ()
{
  _sheetEnd2->setVisible (false);
}

void
EndStateM2::pause ()
{
}

void
EndStateM2::resume ()
{

}

bool EndStateM2::frameStarted (const Ogre::FrameEvent & evt)
{
  return true;
}

bool EndStateM2::frameEnded (const Ogre::FrameEvent & evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
EndStateM2::keyPressed (const OIS::KeyEvent & e)
{
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P)
    {
      _sheetEnd2->setVisible (false);
      popState ();
    }
}

void
EndStateM2::keyReleased (const OIS::KeyEvent & e)
{
}

void
EndStateM2::mouseMoved (const OIS::MouseEvent & e)
{
}

void
EndStateM2::mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

void
EndStateM2::mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

EndStateM2 *EndStateM2::getSingletonPtr ()
{
  return msSingleton;
}

EndStateM2 &EndStateM2::getSingleton ()
{
  assert (msSingleton);
  return *msSingleton;
}







void 
EndStateM2::createSceneEnd () 
{
  created = true; 
  CEGUI::ImagesetManager::getSingleton ().createFromImageFile ("EndBG2", "Player2Wins.png");
  _sheetEnd2 = CEGUI::WindowManager::getSingleton ().createWindow ("TaharezLook/StaticImage", "End2");
  _sheetEnd2->setArea (CEGUI::URect (cegui_reldim (0), cegui_reldim (0), cegui_reldim (1), cegui_reldim (1)));
  _sheetEnd2->setProperty ("FrameEnabled", "false");
  //_sheetEnd2->setProperty("BackgroundEnabled", "true");
  _sheetEnd2->setProperty ("Image", "set:EndBG2 image:full_image");
  CEGUI::Window * layout = CEGUI::WindowManager::getSingleton ().loadWindowLayout ("InsertScore2.layout");
  //CEGUI::Window * ContinueButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Continue");  
  //ContinueButton->subscribeEvent (CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber (&IntroState::submitScore,this));
  CEGUI::Window * MainMenuButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS2/GameOver/Menu"); 
  MainMenuButton->subscribeEvent (CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber (&EndStateM2::backmenu,this));
  CEGUI::Window * restartButton =CEGUI::WindowManager::getSingleton ().getWindow ("RootHS2/GameOver/Continue");
  restartButton->subscribeEvent (CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber (&EndStateM2::restart,this));
  
  _sheetEnd2->addChildWindow (layout);
  _sheetEnd2->setVisible (true);
} 

std::ostream& EndStateM2::operator << (std::ostream &o)
{
 o << "Endstate 1\n"; 
  return o;
}


bool EndStateM2::backmenu (const CEGUI::EventArgs & e) 
{ 
    // Transición al siguiente estado.
      _sheetEnd2->setVisible (false);
      popState ();
  //pushState (PlayState::getSingletonPtr ());
return true;
}

bool EndStateM2::restart (const CEGUI::EventArgs & e) 
{ 
    // Transición al siguiente estado.
      _sheetEnd2->setVisible (false);
      //popState ();
      changeState(MultiPlayState::getSingletonPtr ());
return true;
}