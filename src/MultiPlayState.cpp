#include "MultiPlayState.h"
#include "EndState.h"
#include "EndStateM1.h"
#include "EndStateM2.h"
#include "PauseState.h"
#include "Canon.h"
#include "GameState.h"

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include <SDL.h>
#include <SDL/SDL_mixer.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

using namespace Ogre;

template <> MultiPlayState * Ogre::Singleton < MultiPlayState >::msSingleton = 0;

void
MultiPlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr ();
  _lSceneNode.clear();
  _l2SceneNode.clear();

  // Se recupera el gestor de escena y la cámara.
  //_sceneMgr = _root->getSceneManager("SceneManager");
  //_camera = _sceneMgr->getCamera("IntroCamera");
  //_viewport = _root->getAutoCreatedWindow()->getViewport(0);
  _sceneMgr = _root->createSceneManager (Ogre::ST_GENERIC, "SceneManager");
  //_sceneMgr2 = _root->createSceneManager (Ogre::ST_GENERIC, "SceneManager2");
  _camera = _sceneMgr->createCamera ("IntroCamera");
  _camera2 = _sceneMgr->createCamera ("IntroCamera2");
  _camera3 = _sceneMgr->createCamera("HudCamera");
  _camera3->lookAt (Ogre::Vector3(500,500,0));
  cameraShoot = false;
  camera2Shoot = false;
  //_camera->setWindow (0.25f, 0.0f, 0.75f, 1.0f);
  
  lOverlaySuperContainer = dynamic_cast<Ogre::OverlayContainer*> (Ogre::OverlayManager::getSingleton().createOverlayElement("Panel","Overlay/RttSceneTete/SuperContainer",false));
  
  health = 0;
  health2 = 0;
  _bala = NULL;

  _canon1 = new Canon_1;
  _canon2 = new Canon_2;

  _camera->setPosition (Ogre::Vector3 (0, 10, 80));
  _camera->lookAt (Ogre::Vector3 (0, 0, 0));
  _camera->setNearClipDistance (5);
  _camera->setFarClipDistance (10000);
  _camera->setAutoAspectRatio(true);
  _viewport = _root->getAutoCreatedWindow ()->addViewport (_camera, 0, 0.0f, 0.0f, 0.5f, 1.0f);
  _viewport->setOverlaysEnabled(false);
  
  _camera2->setPosition (Ogre::Vector3 (0, 10, -80));
  _camera2->lookAt (Ogre::Vector3 (0, 0, 0));
  _camera2->setNearClipDistance (5);
  _camera2->setFarClipDistance (10000);
  _camera2->setAutoAspectRatio(true);
  _viewport2 = _root->getAutoCreatedWindow ()->addViewport (_camera2, 50, 0.5f, 0.0f, 0.5f, 1.0f);
  _viewport2->setOverlaysEnabled(false);
  
  _viewport3 = _root->getAutoCreatedWindow()->addViewport(_camera3, 51, 0.0f, 0.0f, 1.0f, 1.0f);
  _viewport3->setOverlaysEnabled(true);
  _viewport3->setClearEveryFrame(true, FBT_DEPTH);
  
  //_overlayManager = Ogre::OverlayManager::getSingletonPtr();
  //Ogre::Overlay *overlay = _overlayManager->getByName("Info");
  //overlay->show();
  createHUD ();

  // Nuevo background colour.

  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

  _numEntities = 0;		// Numero de Shapes instanciadas
  //_timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto

  //_rCanon = 0;        _r = 15;        _c = 1.5;       _rBase = 0;
  _Kpress = false;
  _Uppress = false;
  _Downpress = false;
  _Rightpress = false;
  _Leftpress = false;
  _Lpress = false;
  
  _Apress = false;
  _Spress = false;
  _Dpress = false;
  _Wpress = false;
  _Bpress = false;
  _Vpress = false;
  maxHealth = 50;


  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer ();
  _debugDrawer->setDrawWireframe (true);
  Ogre::SceneNode * node =
    _sceneMgr->getRootSceneNode ()->createChildSceneNode ("debugNode", Ogre::Vector3::ZERO);
  node->attachObject (static_cast < Ogre::SimpleRenderable * >(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  Ogre::AxisAlignedBox worldBounds =
    AxisAlignedBox (Ogre::Vector3 (-10000, -10000, -10000),
		    Ogre::Vector3 (10000, 10000, 10000));
  Ogre::Vector3 gravity = Ogre::Vector3 (0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld (_sceneMgr, worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);
  _world->setShowDebugShapes (false);	// Muestra los collision shapes

  initSDL();
  
  _simpleEffect = _pSoundFXManager->load ("sounds/Cannon_blast2.ogg");
  _simpleEffect2 = _pSoundFXManager->load ("sounds/Globo_explosion.ogg");
  _mainTrack = _pTrackManager->load ("snaremarch.ogg");
  _mainTrack->play ();
  // Creacion de los elementos iniciales del mundo
  CreateInitialWorld ();

  _exitGame = false;
}

bool MultiPlayState::initSDL () 
{
  _pTrackManager = TrackManager::getSingletonPtr();
  _pSoundFXManager = SoundFXManager::getSingletonPtr();
  /*
  if (SDL_Init (SDL_INIT_AUDIO) < 0)
    return false;
  atexit (SDL_Quit);
  // Inicializando SDL mixer...
  if (Mix_OpenAudio (MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0)
    return false;
  // Llamar a Mix_CloseAudio al terminar.
  atexit (Mix_CloseAudio);
  */
  return true;
}

void
MultiPlayState::exit ()
{
  //OverlayManager::getSingleton().destroy("HUD2");   
  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  pOverlayMgr->destroyOverlayElement("Overlay/RttSceneTete/SuperContainer");
  Ogre::Overlay * overlay = pOverlayMgr->getByName ("HUD2");
  //para hacer el calculo de cuanta barra de vida mostrar de acorde a la vida del jugador
  //Ogre::OverlayElement * elem = pOverlayMgr->getOverlayElement ("lifeBarEmpty");
  overlay->hide ();
  _viewport3->setOverlaysEnabled(false);
  if (!_sceneMgr)
    return;

  delete _world;
  delete _debugDrawer;
  _sceneMgr->clearScene ();
  _root->getAutoCreatedWindow ()->removeAllViewports ();
  _sceneMgr->destroyAllCameras ();
  _root->destroySceneManager (_sceneMgr);
  _shapes.clear ();
  _bodies.clear ();
}

void
MultiPlayState::pause ()
{
}

void
MultiPlayState::resume ()
{
  // Se restaura el background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool MultiPlayState::frameStarted (const Ogre::FrameEvent & evt)
{
  Ogre::Vector3 vt (0, 0, 0);
  //Ogre::Real tSpeed = 20.0;
  deltaT = evt.timeSinceLastFrame;
  //int fps = 1.0 / deltaT;
  _world->stepSimulation (deltaT);
  _canon1 ->decTime (deltaT);
  _canon2 ->decTime (deltaT);
  DetectCollisionDrain();
  //_timeLastObject -= deltaT;

  if (_Kpress && (_canon1 ->getTime () <= 0) && (health > 0)) {
    _bala = _canon1 ->cShoot (_world, health);
    cameraShoot = true;
    _simpleEffect->play();
  }
  
  if (_Vpress && (_canon2 ->getTime () <= 0) && (health2 > 0)) {
    _bala2 = _canon2 ->cShoot (_world, health2);
    camera2Shoot = true;
    _simpleEffect->play();
  }

  //if (_Spress) AddDynamicObject(sheep);
  if (_Uppress)
    _canon1 ->cUp (deltaT);

  if (_Downpress)
    _canon1 ->cDown (deltaT);
  
    if (_Wpress)
    _canon2 ->cUp (deltaT);

  if (_Spress)
    _canon2 ->cDown (deltaT);
  
  

  if (_Lpress && (health < 50))
    {
      health += 4 + rand () % 3;
      if (health > 50)
	health = 50;
      _Lpress = false;
    }
  if (_Bpress && (health2 < 50))
    {
      health2 += 4 + rand () % 3;
      if (health2 > 50)
	health2 = 50;
      _Bpress = false;
    }

  // Rotacion de la base
  if (_Rightpress)
    _canon1 ->cRight (deltaT);


  if (_Leftpress)
    _canon1 ->cLeft (deltaT);
  
  if (_Dpress)
    _canon2 ->cRight (deltaT);


  if (_Apress)
    _canon2 ->cLeft (deltaT);

  //Ogre::OverlayElement * oe;
  //oe = _overlayManager->getOverlayElement("cursor");
  //oe->setLeft(posx);  oe->setTop(posy);

  //oe = _overlayManager->getOverlayElement("fpsInfo");
  //oe->setCaption(Ogre::StringConverter::toString(fps));

  //oe = _overlayManager->getOverlayElement("nEntitiesInfo");
  //oe->setCaption(Ogre::StringConverter::toString(_numEntities));

  updateHUD ();
  compEnd();
  
  return true;
}

bool MultiPlayState::frameEnded (const Ogre::FrameEvent & evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
MultiPlayState::keyPressed (const OIS::KeyEvent & e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P)
    pushState (PauseState::getSingletonPtr());
  if ((e.key == OIS::KC_K))
    _Kpress = true;
  if ((e.key == OIS::KC_L))
    _Lpress = true;
  if (e.key == OIS::KC_G)
    _world->setShowDebugShapes (true);
  if (e.key == OIS::KC_H)
    _world->setShowDebugShapes (false);
  // Altura del canyon
  if (e.key == OIS::KC_UP)
    _Uppress = true;
  if (e.key == OIS::KC_DOWN)
    _Downpress = true;
  // Rotacion de la base
  if (e.key == OIS::KC_RIGHT)
    _Rightpress = true;
  if (e.key == OIS::KC_LEFT)
    _Leftpress = true;
  
  // Altura del canyon2
  if (e.key == OIS::KC_W)
    _Wpress = true;
  if (e.key == OIS::KC_S)
    _Spress = true;
  // Rotacion de la base2
  if (e.key == OIS::KC_D)
    _Dpress = true;
  if (e.key == OIS::KC_A)
    _Apress = true;
    if ((e.key == OIS::KC_V))
    _Vpress = true;
  if ((e.key == OIS::KC_B))
    _Bpress = true;
}

void
MultiPlayState::keyReleased (const OIS::KeyEvent & e)
{
  if (e.key == OIS::KC_ESCAPE)
    {
      popState ();
    }
  if (e.key == OIS::KC_K)
    _Kpress = false;
  if ((e.key == OIS::KC_L))
    _Lpress = false;
  // Altura del canyon
  if (e.key == OIS::KC_UP)
    _Uppress = false;
  if (e.key == OIS::KC_DOWN)
    _Downpress = false;
  // Rotacion de la base
  if (e.key == OIS::KC_RIGHT)
    _Rightpress = false;
  if (e.key == OIS::KC_LEFT)
    _Leftpress = false;
  
  // Altura del canyon2
  if (e.key == OIS::KC_W)
    _Wpress = false;
  if (e.key == OIS::KC_S)
    _Spress = false;
  // Rotacion de la base2
  if (e.key == OIS::KC_D)
    _Dpress = false;
  if (e.key == OIS::KC_A)
    _Apress = false;
    if ((e.key == OIS::KC_V))
    _Vpress = false;
  if ((e.key == OIS::KC_B))
    _Bpress = false;

}

void
MultiPlayState::mouseMoved (const OIS::MouseEvent & e)
{
}

void
MultiPlayState::mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

void
MultiPlayState::mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}


MultiPlayState * MultiPlayState::getSingletonPtr ()
{
  return msSingleton;
}

MultiPlayState & MultiPlayState::getSingleton ()
{
  assert (msSingleton);
  return *msSingleton;
}



void
MultiPlayState::CreateInitialWorld ()
{

   // Shadows
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.2, 0.2, 0.2));
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  
  //Vector3 size = Vector3::ZERO;
  //OgreBulletCollisions::CollisionShape * bodyShape = NULL;
  
    // Creacion de la entidad y del SceneNode ------------------------
  
  
  /*SceneNode *node = _sceneMgr->createSceneNode ("ground");
  
  OgreBulletCollisions::StaticMeshToShapeConverter * trimeshConverter2;
  OgreBulletCollisions::TriangleMeshCollisionShape * Trimesh2;
  OgreBulletDynamics::RigidBody * rigidObject2;
  */
  Plane plane1 (Vector3 (0, 1, 0), 0);	// Normal y distancia
  MeshManager::getSingleton ().createPlane ("p1", ResourceGroupManager:: DEFAULT_RESOURCE_GROUP_NAME, plane1, 
					    2000, 5700, 1, 1, true, 1, 80, 80, Vector3::UNIT_Z);
  SceneNode *node = _sceneMgr->createSceneNode ("ground");
  Entity *groundEnt = _sceneMgr->createEntity ("planeEnt", "p1");
  groundEnt->setMaterialName ("Ground");
  groundEnt->setCastShadows(false);
  node->attachObject (groundEnt);
  _sceneMgr->getRootSceneNode ()->addChild (node);

  //_plano = node;
  
  // Creamos forma de colision para el plano ----------------------- 
  OgreBulletCollisions::CollisionShape * Shape;
  Shape = new OgreBulletCollisions::StaticPlaneCollisionShape (Ogre::Vector3 (0, 1, 0), 0);	// Vector normal y distancia
  OgreBulletDynamics::RigidBody * rigidBodyPlane = new OgreBulletDynamics::RigidBody ("rigidBodyPlane", _world);

  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape (node, Shape, 0.55, 1.1);

  // Anadimos los objetos Shape y RigidBody ------------------------
  _shapes.push_back (Shape);
  _bodies.push_back (rigidBodyPlane);
  
  
  Plane plane2 (Vector3 (0, 1, 0), 0);	// Normal y distancia
  MeshManager::getSingleton ().createPlane ("p2", ResourceGroupManager:: DEFAULT_RESOURCE_GROUP_NAME, plane1, 
					    2000, 5700, 1, 1, true, 1, 80, 80, Vector3::UNIT_Z);
  SceneNode *node2 = _sceneMgr->createSceneNode ("groundPhys");
  Entity *groundEnt2 = _sceneMgr->createEntity ("planeEntPhys", "p2");
  //groundEnt->setMaterialName ("Ground");
  node2->attachObject (groundEnt2);
  _sceneMgr->getRootSceneNode ()->addChild (node2);

  _plano = node2;
  
  OgreBulletCollisions::StaticMeshToShapeConverter * trimeshConverter2 = new
    OgreBulletCollisions::StaticMeshToShapeConverter (groundEnt2);

  OgreBulletCollisions::TriangleMeshCollisionShape * Trimesh2 =
    trimeshConverter2->createTrimesh ();

  OgreBulletDynamics::RigidBody * rigidObject2 = new OgreBulletDynamics::RigidBody ("groundPhys", _world);
  rigidObject2->setShape (node2, Trimesh2, 0.5, 0.5, 0,
			  Ogre::Vector3::ZERO, Quaternion::IDENTITY);
  
  
  

  // Anadimos el SKYBOX fase pruebas
  // sintaxis mSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox", 10);
  _sceneMgr->setSkyBox (true, "Scene/Skybox", 10);


  // cuboejes para aclaraciones
  /*
  Entity *cuboejes = _sceneMgr->createEntity ("cuboejes", "cuboejes.mesh");
  SceneNode *node1 = _sceneMgr->getRootSceneNode ()->createChildSceneNode ();
  node1->attachObject (cuboejes);
  node1->translate (5, 1, 0);
  */

  /*/ Creamos objeto Castle OK
     Entity * Castle = _sceneMgr->createEntity("Castle", "Castle.mesh");
     SceneNode *Castle_node = _sceneMgr->getRootSceneNode()->createChildSceneNode();
     Castle_node->attachObject(Castle);
     Castle_node->translate(10,0,-50);
     //Castle_node->setScale(5,5,5);

     // Colisiones para el Castle NO-OK
     OgreBulletCollisions::CollisionShape *Shape2;
     Shape2 = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0,1,0), 0);   // Vector normal y distancia
     OgreBulletDynamics::RigidBody *rigidBodyCastle = new OgreBulletDynamics::RigidBody("rigidBodyCastle", _world);
   */

     // Let's Go Light
    Ogre::SceneNode* node_light1 = node->createChildSceneNode("node_light1");
    node_light1->setPosition(0,250,0);
     // Create a light with the name Light1 and tell Ogre3D it's a Spot light
    Ogre::Light* light1 = _sceneMgr->createLight("light1");
    light1->setType(Ogre::Light::LT_DIRECTIONAL);

    light1->setDiffuseColour(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
    light1->setSpecularColour(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
    light1->setDirection(Ogre::Vector3(0,-1,0));
   

  
  // Globo 1
  Entity *Globo = _sceneMgr->createEntity ("Globo1", "globo.mesh");
  Globo->setCastShadows(true);
  SceneNode *Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo1");
  Globo_node->attachObject (Globo);
  Globo_node->translate (10, 20, -55);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();
  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo1", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (10, 20, -55), Quaternion::IDENTITY);
  _lSceneNode.push_back(Globo_node);

  // Globo 2
  Globo = _sceneMgr->createEntity ("Globo2", "globo.mesh");
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo2");
  Globo_node->attachObject (Globo);
  Globo_node->translate (-14, 19, -55);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();
  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo2", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (-14, 19, -55), Quaternion::IDENTITY);
  _lSceneNode.push_back(Globo_node);

  
  
  
  // Globo 3
  Globo = _sceneMgr->createEntity ("Globo3", "globo.mesh");
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo3");
  Globo_node->attachObject (Globo);
  Globo_node->translate (-1, 22, -55);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);

  Trimesh2 = trimeshConverter2->createTrimesh ();

  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo3", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (-1, 22, -55), Quaternion::IDENTITY);
  _lSceneNode.push_back(Globo_node);
  
  
  
  
  // Globo 4
  Globo = _sceneMgr->createEntity ("Globo4", "globo.mesh");
  Globo->setCastShadows(true);
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo4");
  Globo_node->attachObject (Globo);
  Globo_node->translate (10, 20, 55);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();
  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo4", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (10, 20, 55), Quaternion::IDENTITY);
  _l2SceneNode.push_back(Globo_node);

  // Globo 5
  Globo = _sceneMgr->createEntity ("Globo5", "globo.mesh");
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo5");
  Globo_node->attachObject (Globo);
  Globo_node->translate (-14, 19, 55);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();
  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo5", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (-14, 19, 55), Quaternion::IDENTITY);
  _l2SceneNode.push_back(Globo_node);
  
  // Globo 6
  Globo = _sceneMgr->createEntity ("Globo6", "globo.mesh");
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo6");
  Globo_node->attachObject (Globo);
  Globo_node->translate (-1, 22, 55);
  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();

  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo6", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (-1, 22, 55), Quaternion::IDENTITY);
  _l2SceneNode.push_back(Globo_node);

  
  
  // Ogre Terrain
  /*
     mTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();
     mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(_sceneMgr, Ogre::Terrain::ALIGN_X_Z, 513, 12000.0f);
     mTerrainGroup->setFilenameConvention(Ogre::String("BasicTutorial3Terrain"), Ogre::String("dat"));
     mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);

   */
  //for (long x = 0; x <= 0; ++x)
  //   for (long y = 0; y <= 0; ++y)
  //       defineTerrain(x, y);

  // sync load since we want everything in place when we start


  //mTerrainGroup->loadAllTerrains(true);
}


void MultiPlayState::createHUD ()
{
  
  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  Ogre::Overlay * overlay = pOverlayMgr->getByName ("HUD2");
  
  //Ogre::Overlay * lOverlay = Ogre::OverlayManager::getSingleton().create("Overlay/RttSceneTete");
  lOverlaySuperContainer->setParameter("transparent","true"); // il est invisible (pas de material).
  lOverlaySuperContainer->show();// si on veut voir ses "enfants", il faut que lui meme soit 'visible'.
  overlay->add2D(lOverlaySuperContainer);

  
  //para hacer el calculo de cuanta barra de vida mostrar de acorde a la vida del jugador
  //Ogre::OverlayElement * elem = pOverlayMgr->getOverlayElement ("lifeBarEmpty3");
  overlay->show ();
  //MUY IMPORTANTE: me falta destruir el overlay una vez que salimos del juego al menu
}

//
void MultiPlayState::updateHUD ()
{

  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  //actualizar barra de vida
  Ogre::OverlayElement * elem = pOverlayMgr->getOverlayElement ("lifeBarEmpty3");
  int barSize = elem->getHeight ();

  float hpRatio = health / maxHealth;
  float barWidth = barSize - barSize * hpRatio;

  Ogre::OverlayElement * healthBar = pOverlayMgr->getOverlayElement ("lifeBarFull3");
  if (health > 0) {
      healthBar->setHeight ((int) barWidth);
    }
  else if (health == 0) {
      healthBar->setHeight (barSize);
    }
  else if (health <= maxHealth) {
      healthBar->setHeight (barSize);
    }
  float timeWidth = barSize - (_canon1 ->getTime () / 5.0) * barSize;
  elem = pOverlayMgr->getOverlayElement ("lifeBarFull4");
  if (timeWidth < 176) {
      elem->setHeight ((int) timeWidth);
    }
  else {
      elem->setHeight (barSize);
    }
    
    
  float hpRatio2 = health2 / maxHealth;
  float barWidth2 = barSize - barSize * hpRatio2;

  Ogre::OverlayElement * healthBar2 = pOverlayMgr->getOverlayElement ("lifeBarFull5");
  if (health2 > 0) {
      healthBar2->setHeight ((int) barWidth2);
    }
  else if (health2 == 0) {
      healthBar2->setHeight (barSize);
    }
  else if (health2 <= maxHealth) {
      healthBar2->setHeight (barSize);
    }
  float timeWidth2 = barSize - (_canon2 ->getTime () / 5.0) * barSize;
  elem = pOverlayMgr->getOverlayElement ("lifeBarFull6");
  if (timeWidth2 < 176) {
      elem->setHeight ((int) timeWidth2);
    }
  else {
      elem->setHeight (barSize);
    }



}


void MultiPlayState::DetectCollisionDrain() {
  btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
  int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i=0;i<numManifolds;i++) 
    {
      btPersistentManifold* contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
      btCollisionObject* obA = (btCollisionObject*) contactManifold->getBody0();
      btCollisionObject* obB = (btCollisionObject*) contactManifold->getBody1();
    
      
      OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
      OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);
      
      for(_iSceneNode = _lSceneNode.begin(); _iSceneNode != _lSceneNode.end(); ++_iSceneNode)
      {
	Ogre::SceneNode* drain = *_iSceneNode;

	OgreBulletCollisions::Object *obDrain = _world->findObject(drain);
	

	if ((obOB_A == obDrain) || (obOB_B == obDrain)) 
	{
	  Ogre::SceneNode* node = NULL;
	  if ((obOB_A != obDrain) && (obOB_A)) {
	    node = obOB_B->getRootNode(); delete obOB_B;
	    _iSceneNode = _lSceneNode.erase(_iSceneNode);
	    _simpleEffect2->play();
	  }
	  else if ((obOB_B != obDrain) && (obOB_B)) {
	    node = obOB_A->getRootNode(); delete obOB_A;
	    _iSceneNode = _lSceneNode.erase(_iSceneNode);
	    _simpleEffect2->play();
	  }
	  if (node) {
	    _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());
	  }
	}
      }
      
      for(_iSceneNode = _l2SceneNode.begin(); _iSceneNode != _l2SceneNode.end(); ++_iSceneNode)
      {
	Ogre::SceneNode* drain = *_iSceneNode;

	OgreBulletCollisions::Object *obDrain = _world->findObject(drain);
	

	if ((obOB_A == obDrain) || (obOB_B == obDrain)) 
	{
	  Ogre::SceneNode* node = NULL;
	  if ((obOB_A != obDrain) && (obOB_A)) {
	    node = obOB_B->getRootNode(); delete obOB_B;
	    _iSceneNode = _l2SceneNode.erase(_iSceneNode);
	    _simpleEffect2->play();
	  }
	  else if ((obOB_B != obDrain) && (obOB_B)) {
	    node = obOB_A->getRootNode(); delete obOB_A;
	    _iSceneNode = _l2SceneNode.erase(_iSceneNode);
	    _simpleEffect2->play();
	  }
	  if (node) {
	    _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());
	  }
	}
      }
      
      
	
      OgreBulletCollisions::Object *obBala = _world->findObject(_bala);
      OgreBulletCollisions::Object *obPLano = _world->findObject(_plano);
      
      if (((obOB_A == obPLano) || (obOB_B == obPLano)) && ((obOB_A == obBala) || (obOB_B == obBala))) 
      {
	  if ((obOB_B == obBala) && (obOB_A == obPLano)) {
	    resetCamera();
	  }
	  else if ((obOB_A == obBala) && (obOB_B == obPLano)) {
	    resetCamera();
	  }
      }   
      
      obBala = _world->findObject(_bala2);
      //OgreBulletCollisions::Object *obPLano = _world->findObject(_plano);
      
      if (((obOB_A == obPLano) || (obOB_B == obPLano)) && ((obOB_A == obBala) || (obOB_B == obBala))) 
      {
	  if ((obOB_B == obBala) && (obOB_A == obPLano)) {
	    resetCamera2();
	  }
	  else if ((obOB_A == obBala) && (obOB_B == obPLano)) {
	    resetCamera2();
	  }
      }
  }
}

void MultiPlayState::compEnd() {
  
 if (_lSceneNode.empty() && (_canon1 ->getTime() <= 0)) {
   changeState(EndStateM1::getSingletonPtr());
  
 }
  if (_l2SceneNode.empty() && (_canon2 ->getTime() <= 0)) {
   changeState(EndStateM2::getSingletonPtr());
 }
  
  
}

std::ostream& MultiPlayState::operator << (std::ostream &o)
{
 o << "MultiPlayState 1\n"; 
  return o;
}

void MultiPlayState::resetCamera() 
{
  _camera->detachFromParent(); 
  //_bala->removeAndDestroyAllChildren();  
  _camera->setPosition (Ogre::Vector3 (0, 10, 80));
  _camera->lookAt (Ogre::Vector3 (0, 0, 0));
  _bala = NULL;
  
  _canon1 ->resetCam();
  
}

void MultiPlayState::resetCamera2() 
{
  _camera2->detachFromParent(); 
  //_bala2->removeAndDestroyAllChildren();  
  _camera2->setPosition (Ogre::Vector3 (0, 10, -80));
  _camera2->lookAt (Ogre::Vector3 (0, 0, 0));
  _canon2 ->resetCam();
  _bala2 = NULL;
  
  
  
}
