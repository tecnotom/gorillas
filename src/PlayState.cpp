#include "PlayState.h"
#include "EndState.h"
#include "PauseState.h"
#include "Canon.h"
#include "GameState.h"

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include <SDL.h>
#include <SDL/SDL_mixer.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

using namespace Ogre;

template <> PlayState * Ogre::Singleton < PlayState >::msSingleton = 0;

void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr ();


  // Se recupera el gestor de escena y la cámara.
  //_sceneMgr = _root->getSceneManager("SceneManager");
  //_camera = _sceneMgr->getCamera("IntroCamera");
  //_viewport = _root->getAutoCreatedWindow()->getViewport(0);
  _sceneMgr = _root->createSceneManager (Ogre::ST_GENERIC, "SceneManager");
  _camera = _sceneMgr->createCamera ("IntroCamera");
  cameraShoot = false;
  
  health = 0;
  _bala = NULL;

  _canon = new Canon;

  _camera->setPosition (Ogre::Vector3 (0, 10, 30));
  _camera->lookAt (Ogre::Vector3 (0, 0, -50));
  _camera->setNearClipDistance (5);
  _camera->setFarClipDistance (10000);

  _viewport = _root->getAutoCreatedWindow ()->addViewport (_camera, 0, 0.0f, 0.0f, 1.0f, 1.0f);


  //_overlayManager = Ogre::OverlayManager::getSingletonPtr();
  //Ogre::Overlay *overlay = _overlayManager->getByName("Info");
  //overlay->show();
  createHUD ();

  // Nuevo background colour.

  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

  _numEntities = 0;		// Numero de Shapes instanciadas
  //_timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto

  //_rCanon = 0;        _r = 15;        _c = 1.5;       _rBase = 0;
  _Bpress = false;
  _Spress = false;
  _Uppress = false;
  _Downpress = false;
  _Rightpress = false;
  _Leftpress = false;
  _Lpress = false;
  maxHealth = 50;


  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer ();
  _debugDrawer->setDrawWireframe (true);
  Ogre::SceneNode * node =
    _sceneMgr->getRootSceneNode ()->createChildSceneNode ("debugNode", Ogre::Vector3::ZERO);
  node->attachObject (static_cast < Ogre::SimpleRenderable * >(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  Ogre::AxisAlignedBox worldBounds =
    AxisAlignedBox (Ogre::Vector3 (-10000, -10000, -10000),
		    Ogre::Vector3 (10000, 10000, 10000));
  Ogre::Vector3 gravity = Ogre::Vector3 (0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld (_sceneMgr, worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);
  _world->setShowDebugShapes (false);	// Muestra los collision shapes
  initSDL();
  
  _simpleEffect = _pSoundFXManager->load ("sounds/Cannon_blast2.ogg");
  _simpleEffect2 = _pSoundFXManager->load ("sounds/Globo_explosion.ogg");
  
  _gameTrack = _pTrackManager->load ("snaremarch.ogg");
  _gameTrack->play ();
  // Creacion de los elementos iniciales del mundo
  CreateInitialWorld ();

  _exitGame = false;
}

bool PlayState::initSDL () 
{
  _pTrackManager = TrackManager::getSingletonPtr();
  _pSoundFXManager = SoundFXManager::getSingletonPtr();
  /*
  if (SDL_Init (SDL_INIT_AUDIO) < 0)
    return false;
  atexit (SDL_Quit);
  // Inicializando SDL mixer...
  if (Mix_OpenAudio (MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0)
    return false;
  // Llamar a Mix_CloseAudio al terminar.
  atexit (Mix_CloseAudio);
  */
  return true;
}

void
PlayState::exit ()
{
  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  Ogre::Overlay * overlay = pOverlayMgr->getByName ("HUD");
  //para hacer el calculo de cuanta barra de vida mostrar de acorde a la vida del jugador
  //Ogre::OverlayElement * elem = pOverlayMgr->getOverlayElement ("lifeBarEmpty");
  overlay->hide ();
  if (!_sceneMgr)
    return;

  delete _world;
  delete _debugDrawer;
  _lSceneNode.clear();
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports ();
  _sceneMgr->destroyAllCameras();
  _sceneMgr->getRootSceneNode()->removeAndDestroyAllChildren();
  _root->destroySceneManager (_sceneMgr);
  _shapes.clear ();
  _bodies.clear ();
}

void
PlayState::pause ()
{
}

void
PlayState::resume ()
{
  // Se restaura el background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool PlayState::frameStarted (const Ogre::FrameEvent & evt)
{
  Ogre::Vector3 vt (0, 0, 0);
  //Ogre::Real tSpeed = 20.0;
  deltaT = evt.timeSinceLastFrame;
  //int fps = 1.0 / deltaT;
  _world->stepSimulation (deltaT);
  _canon->decTime (deltaT);
  DetectCollisionDrain();
  //_timeLastObject -= deltaT;

  if (_Bpress && (_canon->getTime () <= 0) && (health > 0)) {
    _bala = _canon->cShoot (_world, health);
    cameraShoot = true;
    _simpleEffect->play();
  }

  //if (_Spress) AddDynamicObject(sheep);
  if (_Uppress)
    _canon->cUp (deltaT);

  if (_Downpress)
    _canon->cDown (deltaT);

  if (_Lpress && (health < 50))
    {
      health += 4 + rand () % 3;
      if (health > 50)
	health = 50;
      _Lpress = false;
    }

  // Rotacion de la base
  if (_Rightpress)
    _canon->cRight (deltaT);


  if (_Leftpress)
    _canon->cLeft (deltaT);

  //Ogre::OverlayElement * oe;
  //oe = _overlayManager->getOverlayElement("cursor");
  //oe->setLeft(posx);  oe->setTop(posy);

  //oe = _overlayManager->getOverlayElement("fpsInfo");
  //oe->setCaption(Ogre::StringConverter::toString(fps));

  //oe = _overlayManager->getOverlayElement("nEntitiesInfo");
  //oe->setCaption(Ogre::StringConverter::toString(_numEntities));

  updateHUD ();
  compEnd();
  
  return true;
}

bool PlayState::frameEnded (const Ogre::FrameEvent & evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
PlayState::keyPressed (const OIS::KeyEvent & e)
{
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P)
    pushState (PauseState::getSingletonPtr());
  if ((e.key == OIS::KC_K)) 
    _Bpress = true;
  if ((e.key == OIS::KC_L))
    _Lpress = true;
  if ((e.key == OIS::KC_S))
    _Spress = true;
  if (e.key == OIS::KC_G)
    _world->setShowDebugShapes (true);
  if (e.key == OIS::KC_H)
    _world->setShowDebugShapes (false);
  // Altura del canyon
  if (e.key == OIS::KC_UP)
    _Uppress = true;
  if (e.key == OIS::KC_DOWN)
    _Downpress = true;
  // Rotacion de la base
  if (e.key == OIS::KC_RIGHT)
    _Rightpress = true;
  if (e.key == OIS::KC_LEFT)
    _Leftpress = true;
}

void
PlayState::keyReleased (const OIS::KeyEvent & e)
{
  if (e.key == OIS::KC_ESCAPE)
    {
      popState ();
    }
  if (e.key == OIS::KC_K)
    _Bpress = false;
  if (e.key == OIS::KC_S)
    _Spress = false;
  if ((e.key == OIS::KC_L))
    _Lpress = false;
  // Altura del canyon
  if (e.key == OIS::KC_UP)
    _Uppress = false;
  if (e.key == OIS::KC_DOWN)
    _Downpress = false;
  // Rotacion de la base
  if (e.key == OIS::KC_RIGHT)
    _Rightpress = false;
  if (e.key == OIS::KC_LEFT)
    _Leftpress = false;

}

void
PlayState::mouseMoved (const OIS::MouseEvent & e)
{
}

void
PlayState::mousePressed (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

void
PlayState::mouseReleased (const OIS::MouseEvent & e, OIS::MouseButtonID id)
{
}

PlayState *
PlayState::getSingletonPtr ()
{
  return msSingleton;
}

PlayState & PlayState::getSingleton ()
{
  assert (msSingleton);
  return *msSingleton;
}


void
PlayState::CreateInitialWorld ()
{

   // Shadows
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.2, 0.2, 0.2));
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  
  //Vector3 size = Vector3::ZERO;
  //OgreBulletCollisions::CollisionShape * bodyShape = NULL;
  
    // Creacion de la entidad y del SceneNode ------------------------
  Plane plane1 (Vector3 (0, 1, 0), 0);	// Normal y distancia
  MeshManager::getSingleton ().createPlane ("p1", ResourceGroupManager:: DEFAULT_RESOURCE_GROUP_NAME, plane1, 
					    2000, 5700, 1, 1, true, 1, 80, 80, Vector3::UNIT_Z);
  SceneNode *node = _sceneMgr->createSceneNode ("ground");
  Entity *groundEnt = _sceneMgr->createEntity ("planeEnt", "p1");
  groundEnt->setMaterialName ("Ground");
  groundEnt->setCastShadows(false);
  node->attachObject (groundEnt);
  _sceneMgr->getRootSceneNode ()->addChild (node);

  //_plano = node;
  
  // Creamos forma de colision para el plano ----------------------- 
  OgreBulletCollisions::CollisionShape * Shape;
  Shape = new OgreBulletCollisions::StaticPlaneCollisionShape (Ogre::Vector3 (0, 1, 0), 0);	// Vector normal y distancia
  OgreBulletDynamics::RigidBody * rigidBodyPlane = new OgreBulletDynamics::RigidBody ("rigidBodyPlane", _world);

  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape (node, Shape, 0.55, 1.1);

  // Anadimos los objetos Shape y RigidBody ------------------------
  _shapes.push_back (Shape);
  _bodies.push_back (rigidBodyPlane);
  
  
  Plane plane2 (Vector3 (0, 1, 0), 0);	// Normal y distancia
  MeshManager::getSingleton ().createPlane ("p2", ResourceGroupManager:: DEFAULT_RESOURCE_GROUP_NAME, plane1, 
					    2000, 5700, 1, 1, true, 1, 80, 80, Vector3::UNIT_Z);
  SceneNode *node2 = _sceneMgr->createSceneNode ("groundPhys");
  Entity *groundEnt2 = _sceneMgr->createEntity ("planeEntPhys", "p2");
  //groundEnt->setMaterialName ("Ground");
  node2->attachObject (groundEnt2);
  _sceneMgr->getRootSceneNode ()->addChild (node2);

  _plano = node2;
  
  OgreBulletCollisions::StaticMeshToShapeConverter * trimeshConverter2 = new
    OgreBulletCollisions::StaticMeshToShapeConverter (groundEnt2);

  OgreBulletCollisions::TriangleMeshCollisionShape * Trimesh2 =
    trimeshConverter2->createTrimesh ();

  OgreBulletDynamics::RigidBody * rigidObject2 = new OgreBulletDynamics::RigidBody ("groundPhys", _world);
  rigidObject2->setShape (node2, Trimesh2, 0.5, 0.5, 0,
			  Ogre::Vector3::ZERO, Quaternion::IDENTITY);
  
  
  
  

  // Anadimos el SKYBOX fase pruebas
  // sintaxis mSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox", 10);
  _sceneMgr->setSkyBox (true, "Scene/Skybox", 10);


  // cuboejes para aclaraciones
  /*
  Entity *cuboejes = _sceneMgr->createEntity ("cuboejes", "cuboejes.mesh");
  SceneNode *node1 = _sceneMgr->getRootSceneNode ()->createChildSceneNode ();
  node1->attachObject (cuboejes);
  node1->translate (5, 1, 0);
  */

  /*/ Creamos objeto Castle OK
     Entity * Castle = _sceneMgr->createEntity("Castle", "Castle.mesh");
     SceneNode *Castle_node = _sceneMgr->getRootSceneNode()->createChildSceneNode();
     Castle_node->attachObject(Castle);
     Castle_node->translate(10,0,-50);
     //Castle_node->setScale(5,5,5);

     // Colisiones para el Castle NO-OK
     OgreBulletCollisions::CollisionShape *Shape2;
     Shape2 = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0,1,0), 0);   // Vector normal y distancia
     OgreBulletDynamics::RigidBody *rigidBodyCastle = new OgreBulletDynamics::RigidBody("rigidBodyCastle", _world);
   */

     // Let's Go Light
    Ogre::SceneNode* node_light1 = node->createChildSceneNode("node_light1");
    node_light1->setPosition(0,20,0);
     // Create a light with the name Light1 and tell Ogre3D it's a Spot light
    Ogre::Light* light1 = _sceneMgr->createLight("light1");
    light1->setType(Ogre::Light::LT_DIRECTIONAL);

    light1->setDiffuseColour(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
    light1->setSpecularColour(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
    light1->setDirection(Ogre::Vector3(0,-0.75,-0.75));
   

  
  // Globo 1
  Entity *Globo = _sceneMgr->createEntity ("Globo1", "globo.mesh");
  Globo->setCastShadows(true);
  SceneNode *Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo1");
  Globo_node->attachObject (Globo);
  Globo_node->translate (10, 20, -50);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();
  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo1", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (10, 20, -50), Quaternion::IDENTITY);
  _lSceneNode.push_back(Globo_node);

  // Globo 2
  Globo = _sceneMgr->createEntity ("Globo2", "globo.mesh");
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo2");
  Globo_node->attachObject (Globo);
  Globo_node->translate (-14, 15, -65);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);
  Trimesh2 = trimeshConverter2->createTrimesh ();
  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo2", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (-14, 15, -65), Quaternion::IDENTITY);
  _lSceneNode.push_back(Globo_node);

  // Globo 3
  Globo = _sceneMgr->createEntity ("Globo3", "globo.mesh");
  Globo_node = _sceneMgr->getRootSceneNode ()->createChildSceneNode ("Globo3");
  Globo_node->attachObject (Globo);
  Globo_node->translate (-1, 7, -45);

  trimeshConverter2 = new OgreBulletCollisions::StaticMeshToShapeConverter (Globo);

  Trimesh2 = trimeshConverter2->createTrimesh ();

  rigidObject2 = new OgreBulletDynamics::RigidBody ("Globo3", _world);
  rigidObject2->setShape (Globo_node, Trimesh2, 0.5, 0.5, 0, Ogre::Vector3 (-1, 7, -45), Quaternion::IDENTITY);
  _lSceneNode.push_back(Globo_node);

  // Ogre Terrain
  /*
     mTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();
     mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(_sceneMgr, Ogre::Terrain::ALIGN_X_Z, 513, 12000.0f);
     mTerrainGroup->setFilenameConvention(Ogre::String("BasicTutorial3Terrain"), Ogre::String("dat"));
     mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);

   */
  //for (long x = 0; x <= 0; ++x)
  //   for (long y = 0; y <= 0; ++y)
  //       defineTerrain(x, y);

  // sync load since we want everything in place when we start


  //mTerrainGroup->loadAllTerrains(true);
}


void PlayState::createHUD ()
{

  Ogre::OverlayManager * pOverlayMgr = Ogre::OverlayManager::getSingletonPtr ();
  Ogre::Overlay * overlay = pOverlayMgr->getByName ("HUD");
  //para hacer el calculo de cuanta barra de vida mostrar de acorde a la vida del jugador
  //Ogre::OverlayElement * elem = pOverlayMgr->getOverlayElement ("lifeBarEmpty");
  overlay->show ();
  //MUY IMPORTANTE: me falta destruir el overlay una vez que salimos del juego al menu
}

//
void PlayState::updateHUD ()
{

  Ogre::OverlayManager * pOverlayMgr =
    Ogre::OverlayManager::getSingletonPtr ();
  //actualizar barra de vida
  Ogre::OverlayElement * elem =
    pOverlayMgr->getOverlayElement ("lifeBarEmpty");
  int barSize = elem->getHeight ();

  float hpRatio = health / maxHealth;
  float barWidth = barSize - barSize * hpRatio;

  Ogre::OverlayElement * healthBar =
    pOverlayMgr->getOverlayElement ("lifeBarFull");

  if (health > 0)
    {
      healthBar->setHeight ((int) barWidth);
    }
  else if (health == 0)
    {
      healthBar->setHeight (barSize);
    }
  else if (health <= maxHealth)
    {
      healthBar->setHeight (barSize);
    }
  float timeWidth = barSize - (_canon->getTime () / 5.0) * barSize;

  elem = pOverlayMgr->getOverlayElement ("lifeBarEmpty2");

  if (timeWidth < 176)
    {
      elem->setHeight ((int) timeWidth);
    }
  else
    {
      elem->setHeight (barSize);
    }



}


void PlayState::DetectCollisionDrain() {
  btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
  int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i=0;i<numManifolds;i++) 
    {
      btPersistentManifold* contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
      btCollisionObject* obA = (btCollisionObject*) contactManifold->getBody0();
      btCollisionObject* obB = (btCollisionObject*) contactManifold->getBody1();
    
      
      OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
      OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);
      
      for(_iSceneNode = _lSceneNode.begin(); _iSceneNode != _lSceneNode.end(); ++_iSceneNode)
      {
	Ogre::SceneNode* drain = *_iSceneNode;

	OgreBulletCollisions::Object *obDrain = _world->findObject(drain);
	

	if ((obOB_A == obDrain) || (obOB_B == obDrain)) 
	{
	  Ogre::SceneNode* node = NULL;
	  if ((obOB_A != obDrain) && (obOB_A)) {
	    node = obOB_B->getRootNode(); delete obOB_B;
	    _iSceneNode = _lSceneNode.erase(_iSceneNode);
	    _simpleEffect2->play();
	  }
	  else if ((obOB_B != obDrain) && (obOB_B)) {
	    node = obOB_A->getRootNode(); delete obOB_A;
	    _iSceneNode = _lSceneNode.erase(_iSceneNode);
	    _simpleEffect2->play();
	  }
	  if (node) {
	    _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());
	  }
	}
      }
	
      OgreBulletCollisions::Object *obBala = _world->findObject(_bala);
      OgreBulletCollisions::Object *obPLano = _world->findObject(_plano);
      
      if (((obOB_A == obPLano) || (obOB_B == obPLano)) && ((obOB_A == obBala) || (obOB_B == obBala))) 
      {
	  if ((obOB_B == obBala) && (obOB_A == obPLano)) {
	    resetCamera();
	  }
	  else if ((obOB_A == obBala) && (obOB_B == obPLano)) {
	    resetCamera();
	  }
      }    
  }
}

void PlayState::compEnd() {
  
 if (_lSceneNode.empty() && (_canon->getTime() <= 0)) {
   changeState(EndState::getSingletonPtr());
  
 }
  
  
  
}

std::ostream& PlayState::operator << (std::ostream &o)
{
 o << "PlayState 1\n"; 
  return o;
}

void PlayState::resetCamera() 
{
  _camera->detachFromParent(); 
  _bala->removeAndDestroyAllChildren();  
  _camera->setPosition (Ogre::Vector3 (0, 10, 30));
  _camera->lookAt (Ogre::Vector3 (0, 0, -50));
  _bala = NULL;
  
  _canon->resetCam();
  
}
